void OnTick()
  {
                  
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   CheckTrailingStop(Ask, Bid);
  
  }


void CheckTrailingStop(double Ask ,double Bid){
   double SL=NormalizeDouble(Ask-1000*_Point,_Digits);
   double SLS=NormalizeDouble(Bid+1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double PositionVolume = PositionGetDouble(POSITION_VOLUME);
         double TP = PositionBuyPrice+2500*_Point;
         double TPS = PositionBuyPrice-5000*_Point;
         int positionType = PositionGetInteger(POSITION_TYPE);//to find if the position is long or short
         if(positionType==0){
            if(PositionBuyPrice+1000*_Point<Ask && CurrentStopLoss<PositionBuyPrice){
               trade.PositionModify(PositionTicket,(PositionBuyPrice+250*_Point),0);   
            }
            if( CurrentStopLoss<SL && CurrentStopLoss>PositionBuyPrice){
               trade.PositionModify(PositionTicket,(CurrentStopLoss+25*_Point),0);
           }
         } 
         if(positionType==1){
            if(PositionBuyPrice-1000*_Point>Bid && CurrentStopLoss>PositionBuyPrice){
               trade.PositionModify(PositionTicket,(PositionBuyPrice-250*_Point),0);   
            }
            if( CurrentStopLoss>SLS && CurrentStopLoss<PositionBuyPrice){
               trade.PositionModify(PositionTicket,(CurrentStopLoss-25*_Point),0);
            } 
         }
        }
     }

}
  // trade.PositionClose(PositionTicket);