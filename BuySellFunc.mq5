#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.01;
input int stop = 1000;
input int take = 1000;

void OnTick(){
                  
  double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
  double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
  //BuyIt(Ask,volume);
  //SellIt(Bid,volume);
}
void BuyIt(double Ask,double volume){
  trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-stop*_Point),//sl
    (Ask+take*_Point),//tp
    NULL
  );
}
void SellIt(double Bid,double volume){
  trade.Sell(
    volume,
    NULL,
    Bid,
    (Bid+stop*_Point),//sl
    (Bid-take*_Point),//tp
    NULL
  );
}

