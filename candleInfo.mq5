void onTick(){
   double highestPrice;
   double high[];
   ArraySetAsSeries(high,true);
   CopyHigh(_Symbol,PERIOD_M30,0,100,high);
   highestPrice = ArrayMaximum(high,0,100);
   
   double lowestPrice;
   double low[];
   ArraySetAsSeries(low,true);
   CopyLow(_Symbol,PERIOD_M30,0,3,low);
   lowestPrice = ArrayMinimum(low,0,3);
}