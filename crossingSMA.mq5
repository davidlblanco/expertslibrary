void OnTick()
  {
   double myMovingAverageArray1[],myMovingAverageArray2[];
   int movingAverageDefinition1 = iMA (_Symbol,_Period,1,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,_Period,100,0,MODE_EMA,PRICE_CLOSE);
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,5,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,5,myMovingAverageArray2);
  
   if(
   (myMovingAverageArray1[0]>myMovingAverageArray2[0])&&
   (myMovingAverageArray1[1]<myMovingAverageArray2[1])
   ){
      Comment("BUY");
   } 
   
  if(
   (myMovingAverageArray1[0]<myMovingAverageArray2[0])&&
   (myMovingAverageArray1[1]>myMovingAverageArray2[1])
   ){
     Comment("SELL");
   } 
  }