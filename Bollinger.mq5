void OnTick()
  {
   double MidBandArray[];
   double UpperBandArray[];
   double LowerBandArray[]; 
      
   ArraySetAsSeries(MidBandArray,true);
   ArraySetAsSeries(UpperBandArray,true);
   ArraySetAsSeries(LowerBandArray,true);
   
   int BBDefinition = iBands(_Symbol,_Period,20,0,2,PRICE_CLOSE);
   
   CopyBuffer(BBDefinition,0,0,3,MidBandArray);
   CopyBuffer(BBDefinition,1,0,3,UpperBandArray);
   CopyBuffer(BBDefinition,2,0,3,LowerBandArray);
   
   double midBandValue = MidBandArray[0];
   double upperBandValue = UpperBandArray[0];
   double lowerBandValue = LowerBandArray[0];
   
   Comment(
      "UPPER: ",upperBandValue,"\n",
      "MID: ",midBandValue,"\n",
      "LOWER",lowerBandValue,"\n"
   );
   
  
   
   
   
  }
