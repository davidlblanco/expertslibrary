
void OnTick()
  {
   if(PositionSelect(_Symbol)==true){
   for(int i=PositionsTotal()-1;i>0;i--)
     {
      ulong PositionTicket = PositionGetTicket(i);
      string PositionSymbol = PositionGetString(POSITION_SYMBOL);
      double PositionPriceOpen = PositionGetDouble(POSITION_PRICE_OPEN);   
      double PositionPriceCurrent = PositionGetDouble(POSITION_PRICE_CURRENT);   
      double PositionProfit = PositionGetDouble(POSITION_PROFIT);   
      int PositionSwap = (int)PositionGetDouble(POSITION_SWAP);   
      int PositionType = PositionGetInteger(POSITION_TYPE);
      double PositionNetProfit = PositionProfit+PositionSwap;
      if(PositionSymbol==_Symbol)
        {
          Comment(
          "Position Number",i,"\n",
          "Position Ticket",PositionTicket,"\n",
          "Position Symbol",PositionSymbol,"\n",
          "Position Profit",PositionProfit,"\n",
          "Position Swap",PositionSwap,"\n",
          "Position Netprofit",PositionNetProfit,"\n",
          "Position Price Open",PositionPriceOpen,"\n",
          "Position Current Price",PositionPriceCurrent,"\n",
          "Position Type",PositionType,"\n"
          );
        }
     }
   }
  }
